Source: mlpack
Section: libs
Priority: optional
Maintainer: Debian Science Maintainers <debian-science-maintainers@alioth-lists.debian.net>
Uploaders: Barak A. Pearlmutter <bap@debian.org>
Build-Depends: cmake, debhelper-compat (= 13),
		pkg-config,
		libcereal-dev,
		libxml2-dev,
		libarmadillo-dev,
		libensmallen-dev (>= 2.10.0),
		libstb-dev,
		dh-python, dh-sequence-python3,
		python3, python3-dev, python3-pandas, python3-numpy, cython3, python3-setuptools,
		python3-pytest-runner,
		txt2man,
Standards-Version: 4.6.2
Rules-Requires-Root: no
Homepage: https://www.mlpack.org/
Vcs-Git: https://salsa.debian.org/science-team/mlpack.git
Vcs-Browser: https://salsa.debian.org/science-team/mlpack

Package: libmlpack-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Depends: ${misc:Depends}, ${python3:Depends},
	 libarmadillo-dev, liblapack-dev, libxml2-dev,
	 libcereal-dev
Suggests: mlpack-doc
Description: intuitive, fast, scalable C++ machine learning library (development libs)
 This package contains the mlpack Library development files.
 .
 Machine Learning Pack (mlpack) is an intuitive, fast, scalable C++
 machine learning library, meant to be a machine learning analog to
 LAPACK.  It aims to implement a wide array of machine learning
 methods and function as a "swiss army knife" for machine learning
 researchers.

Package: python3-mlpack
Architecture: any
Section: python
Depends: ${misc:Depends}, ${shlibs:Depends},
	 ${python3:Depends}
Description: intuitive, fast, scalable C++ machine learning library (Python bindings)
 This package contains Python bindings for the mlpack Library.
 .
 Machine Learning Pack (mlpack) is an intuitive, fast, scalable C++
 machine learning library, meant to be a machine learning analog to
 LAPACK.  It aims to implement a wide array of machine learning
 methods and function as a "swiss army knife" for machine learning
 researchers.

Package: mlpack-bin
Section: science
Architecture: any
Pre-Depends: ${misc:Pre-Depends}
Depends: ${misc:Depends}, ${shlibs:Depends}, ${python3:Depends}
Description: intuitive, fast, scalable C++ machine learning library (binaries)
 This package contains example binaries using the mlpack Library.
 .
 Machine Learning Pack (mlpack) is an intuitive, fast, scalable C++
 machine learning library, meant to be a machine learning analog to
 LAPACK.  It aims to implement a wide array of machine learning
 methods and function as a “swiss army knife” for machine learning
 researchers.

Package: mlpack-doc
Section: doc
Architecture: all
Multi-Arch: foreign
Pre-Depends: ${misc:Pre-Depends}
Depends: ${misc:Depends}, ${python3:Depends}, libjs-jquery
Description: intuitive, fast, scalable C++ machine learning library (documentation)
 This package contains documentation for the mlpack Library.
 .
 Machine Learning Pack (mlpack) is an intuitive, fast, scalable C++
 machine learning library, meant to be a machine learning analog to
 LAPACK.  It aims to implement a wide array of machine learning
 methods and function as a “swiss army knife” for machine learning
 researchers.
